<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Model;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

use TSN\Contact\Api\Model\ContactInterface;
use TSN\Contact\Api\Model\ContactInterfaceFactory;
use TSN\Contact\Api\Model\ContactRepositoryInterface;
use TSN\Contact\Model\ResourceModel\Contact\CollectionFactory;
use TSN\Contact\Model\ResourceModel\Contact as ResourceModel;

class ContactRepository implements ContactRepositoryInterface
{
    /** @var CollectionFactory */
    protected $collectionFactory;

    /** @var ResourceModel */
    protected $resourceModel;

    /** @var ContactInterfaceFactory */
    protected $modelFactory;

    public function __construct(
        ResourceModel $resourceModel,
        CollectionFactory $collectionFactory,
        ContactInterfaceFactory $factory
    )
    {
        $this->resourceModel        = $resourceModel;
        $this->collectionFactory    = $collectionFactory;
        $this->modelFactory         = $factory;
    }

    /**
     * @param int $contactId
     * @return ContactInterface
     */
//    public function get($contactId)
//    {
//        $contact = $this->getContactObject();
//        $contact->setId($contactId);
//
//        return $this->resourceModel->load($contact);
//    }

    public function getById($contactId)
    {
        $contact = $this->modelFactory->create();
        $this->resourceModel->load($contact, $contactId);

        if (!$contact->getId()) {
            throw new NoSuchEntityException(__('Contact with id "%1" does not exist.', $contactId));
        }

        return $contact;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        // TODO: Implement getList() method.
    }

    /**
     * @param ContactInterface $contact
     * @return ContactInterface
     */
    public function save(ContactInterface $contact)
    {
        try {
            $this->resourceModel->save($contact);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
    }

    /**
     * @param ContactInterface $contact
     * @return ContactRepositoryInterface
     */
    public function delete(ContactInterface $contact)
    {
       $this->resourceModel->delete($contact);

       return $this;
    }

    /**
     * @param int $contactId
     * @return ContactRepositoryInterface
     */
    public function deleteById($contactId)
    {
        $contact = $this->getById($contactId);

        return $this->delete($contact);
    }

    /** {@inheritdoc} */
    public function getContactObject()
    {
        return $this->modelFactory->create();
    }
}