<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Base\Block;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Context;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\View\Helper\Js;
use Magento\Framework\View\LayoutFactory;

class Info extends \Magento\Config\Block\System\Config\Form\Fieldset
{
    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $_layoutFactory;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    public function __construct(
        Context $context,
        Session $authSession,
        Js $jsHelper,
        LayoutFactory $layoutFactory,

        \Magento\Framework\App\State $appState,
        array $data = []
    ) {
        parent::__construct($context, $authSession, $jsHelper, $data);
        $this->_layoutFactory = $layoutFactory;
        $this->appState = $appState;
    }

    /**
     * Render fieldset html
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $html = $this->_getHeaderHtml($element);


        $html .= $this->_getGeneralInfo($element);
        $html .= $this->_getMagentoMode($element);

        $html .= $this->_getFooterHtml($element);

        return $html;
    }

    /**
     * @return \Magento\Framework\View\Element\BlockInterface
     */
    protected function _getFieldRenderer()
    {
        if (empty($this->_fieldRenderer)) {
            $layout = $this->_layoutFactory->create();

            $this->_fieldRenderer = $layout->createBlock(
                'Magento\Config\Block\System\Config\Form\Field'
            );
        }

        return $this->_fieldRenderer;
    }

    /**
     * @return mixed
     */
    protected function _getMagentoMode($fieldset)
    {
        $label = __("Magento Mode");
        $mode = $this->appState->getMode();
        $mode = ucfirst($mode);

        $field = $fieldset->addField('magento_mode', 'label', array(
            'name'  => 'dummy',
            'label' => $label,
            'value' => $mode,
        ))->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }

    /**
     * @param $fieldset
     * @return mixed
     */
    protected function _getGeneralInfo($fieldset) {


            $value = '<div class="red">';
            $value .= __('www.tsn-media.com') . "</div>";
            $value .=
                "<a target='_blank' href='https://tsn-media.com/en/'>" .
                __("Learn more") .
                "</a>";


        $label = __('TSN-Media');

        $field = $fieldset->addField('tsn_info', 'label', array(
            'name'  => 'dummy',
            'label' => $label,
            'after_element_html' => $value,
        ))->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }
}
