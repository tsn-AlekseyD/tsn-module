<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Related\Test\Helper;

use PHPUnit\Framework\TestCase;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;

use TSN\Related\Helper\Config;

class ConfigTest extends TestCase
{

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Config
     */
    protected $_config;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Config
     */
    protected $_scopeConfigMock;


    protected function setUp()
    {
        $this->_scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $context = $this->createMock(Context::class);
        $context->expects($this->once())
            ->method('getScopeConfig')
            ->willReturn($this->_scopeConfigMock);

        $this->_config = new Config($context);
    }

    public function testGetRelatedProductQty()
    {
        $this->_scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn(false);

        $result = $this->_config->getRelatedProductQty();

        $this->assertEquals(false, $result);
    }

}
