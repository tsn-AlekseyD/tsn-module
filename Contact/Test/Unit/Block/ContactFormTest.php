<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Test\Unit\Block;

use PHPUnit\Framework\TestCase;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlInterface;

use TSN\Contact\Block\ContactForm;

class ContactFormTest extends TestCase
{
    /**
     * @var ContactForm
     */
    protected $contactform;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject| UrlInterface
     */
    protected $urlBuilderMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject| Context
     */
    protected $context;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->context = $this->createMock(Context::class);

        $this->contactform = $objectManager->getObject(
            ContactForm::class,
            ['context', $this->context]
        );


    }

    /**
     * {@inheritdoc}
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @return void
     */
    public function testGetFormAction()
    {
        $this->urlBuilderMock = $this->createMock(UrlInterface::class);


        $this->urlBuilderMock->expects($this->once())->method('getUrl')->with('tsn_contact/index/contact', ['_secure' => true]);
        $this->contactform->getFormAction();
    }




}