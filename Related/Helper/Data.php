<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Related\Helper;

/**
 * Class Data
 * @package TSN\Related\Helper
 */
class Data extends Config
{
    /**
     * @param array $data
     * @param string $field
     * @return array
     */
    public function getIdsArray(array $data, $field)
    {
        $newData = [];
        foreach ($data as $item) {
            $newData[] = $item[$field];
        }

        return $newData;
    }
}