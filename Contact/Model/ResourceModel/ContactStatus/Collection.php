<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Model\ResourceModel\ContactStatus;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

use TSN\Contact\Model\ContactStatus as Model;
use TSN\Contact\Model\ResourceModel\ContactStatus as ResourceModel;

class Collection extends AbstractCollection
{
    /** {@inheritdoc} */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}