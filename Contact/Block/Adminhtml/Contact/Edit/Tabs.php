<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Block\Adminhtml\Contact\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

use TSN\Contact\Block\Adminhtml\Contact\Edit\Tab\General as GeneralTab;

class Tabs extends WidgetTabs
{
    /** {@inheritdoc} */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('tsn_contact_contact_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Contact information'));
    }
    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'general_info',
            [
                'label' => __('General'),
                'title' => __('General'),
                'content' => $this->getLayout()->createBlock(
                    GeneralTab::class
                )->toHtml(),
                'active' => true
            ]
        );

        return parent::_beforeToHtml();
    }
}
