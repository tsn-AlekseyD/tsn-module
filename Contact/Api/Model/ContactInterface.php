<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Api\Model;

interface ContactInterface
{
    const CACHE_TAG      = 'tsn_contact';

    const REGISTRY_KEY   = 'tsn_contact_contact';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @param int $contactId
     * @return ContactInterface
     */
    public function setId($contactId);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return ContactInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $email
     * @return ContactInterface
     */
    public function setEmail($email);

    /**
     * @return string
     */
    public function getPhone();

    /**
     * @param string $phone
     * @return ContactInterface
     */
    public function setPhone($phone);

    /**
     * @return string
     */
    public function getComment();

    /**
     * @param string $comment
     * @return ContactInterface
     */
    public function setComment($comment);
}
