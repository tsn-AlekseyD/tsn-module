<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Model;


use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

use TSN\Contact\Api\Model\ContactStatusInterface;
use TSN\Contact\Api\Model\ContactStatusInterfaceFactory;
use TSN\Contact\Api\Model\ContactStatusRepositoryInterface;
use TSN\Contact\Model\ResourceModel\ContactStatus\CollectionFactory;
use TSN\Contact\Model\ResourceModel\ContactStatus as RecourceModel;



class ContactStatusRepository implements ContactStatusRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var RecourceModel
     */
    protected $resourceModel;
    /**
     * @var ContactStatusInterfaceFactory
     */
    protected $modelFactory;

    public function __construct(
        CollectionFactory $collectionFactory,
        RecourceModel $resourceModel,
        ContactStatusInterfaceFactory $factory
    ){
        $this->collectionFactory    = $collectionFactory;
        $this->resourceModel        = $resourceModel;
        $this->modelFactory         = $factory;
    }

    /**
     * @param int $contactStatusId
     * @return ContactStatusInterface
     */
    public function get($contactStatusId)
    {
        $contactStatus = $this->modelFactory->create();
        $contactStatus->setId($contactStatusId);

        return $this->resourceModel->load($contactStatus);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        // TODO: Implement getList() method.
    }

    /**
     * @param ContactStatusInterface $contactStatus
     * @return ContactStatusInterface
     */
    public function save(ContactStatusInterface $contactStatus)
    {
        return $this->resourceModel->save($contactStatus);
    }

    /**
     * @param ContactStatusInterface $contactStatus
     * @return ContactStatusRepositoryInterface
     */
    public function delete(ContactStatusInterface $contactStatus)
    {
        $this->resourceModel->delete($contactStatus);

        return $this;
    }

    /**
     * @param int $contactStatusId
     * @return ContactStatusRepositoryInterface
     */
    public function deleteById($contactStatusId)
    {
        $contactStatus = $this->get($contactStatusId);

        return $this->delete($contactStatus);
    }

    /** {@inheritdoc} */
    public function getContactStatusObject()
    {
        return $this->modelFactory->create();
    }
}