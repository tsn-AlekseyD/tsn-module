<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use TSN\Contact\Controller\Index as BaseAction;

class Index extends BaseAction
{
    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}