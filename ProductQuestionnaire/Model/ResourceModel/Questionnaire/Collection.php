<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Model\ResourceModel\Questionnaire;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

use TSN\ProductQuestionnaire\Model\Questionnaire as Model;
use TSN\ProductQuestionnaire\Model\ResourceModel\Questionnaire as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'questionnaire_id';

    /**
     * Collection initialisation
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(Model::class, ResourceModel::class);
    }
}