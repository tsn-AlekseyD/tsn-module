<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

use TSN\Contact\Model\ResourceModel\ContactStatus\CollectionFactory;

class SelectForm extends AbstractHelper
{
    /**
     * @var CollectionFactory;
     */
    protected $contactStatusCollectionFactory;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \TSN\Contact\Model\ResourceModel\ContactStatus\CollectionFactory $contactStatusCollectionFactory
     */

    public function __construct(
        Context $context,
        CollectionFactory $contactStatusCollectionFactory

    ) {
        $this->contactStatusCollectionFactory = $contactStatusCollectionFactory;
        parent::__construct($context);
    }

    public function getStatusCollection()
    {
       return $this->contactStatusCollectionFactory->create();
    }

    public function getReviewStatuses()
    {
        $result = [];
        foreach ($this->getStatusCollection() as $item) {
            $result[] = ['value' => $item->getStatusId(), 'label' => $item->getStatusLabel()];
        }
        return $result;
    }
}