<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Controller\Adminhtml\Group;

use TSN\ProductQuestionnaire\Model\GroupQuestionnaire;

class MassEnable extends MassAction
{
    /**
     * @param GroupQuestionnaire $group
     * @return $this
     */
    protected function massAction(GroupQuestionnaire $group)
    {
        $group->setIsActive(true);
        $this->groupQuestionRepository->save($group);
        return $this;
    }
}
