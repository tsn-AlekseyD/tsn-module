<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Api\Model\Schema;


interface ContactInterface
{
    const TABLE_NAME        = 'tsn_contact';

    const ID_FIELD          = 'contact_id';
    const NAME_FIELD        = 'name';
    const EMAIL_FIELD       = 'email';
    const PHONE_FIELD       = 'phone';
    const COMMENTS_FIELD    = 'comments';
    const STATUS_FIELD      = 'status';
}
