<?php

namespace TSN\ShippingRate\Block\Adminhtml\Order\Create\Shipping\Method;

use TSN\ShippingRate\Model\Carrier;

class Form extends \Magento\Sales\Block\Adminhtml\Order\Create\Shipping\Method\Form
{
    protected $activeMethodRate;

    /** @return string */
    public function getActiveShippingRateMethod()
    {
        $rate = $this->getActiveMethodRate();
        return $rate && $rate->getCarrier() == Carrier::CODE ? $rate->getMethod() : '';
    }

    /** @return float|int|string */
    public function getActiveShippingRatePrice()
    {
        $rate = $this->getActiveMethodRate();
        return $this->getActiveShippingRateMethod() && $rate->getPrice() ? $rate->getPrice() * 1 : '';
    }

    /** @return bool */
    public function isShippingRateActive()
    {
        if (empty($this->activeMethodRate)) {
            $this->activeMethodRate = $this->getActiveMethodRate();
        }

        return $this->activeMethodRate && $this->activeMethodRate->getCarrier() == Carrier::CODE ? true : false;
    }

    /** @return mixed */
    public function getGroupShippingRates()
    {
        $rates = $this->getShippingRates();

        if (array_key_exists(Carrier::CODE, $rates)) {
            if (!$this->isShippingRateActive()) {
                unset($rates[Carrier::CODE]);
            } else {
                $activeRateMethod = $this->getActiveShippingRateMethod();
                foreach ($rates[Carrier::CODE] as $key => $rate) {
                    if ($rate->getMethod() != $activeRateMethod) {
                        unset($rates[Carrier::CODE][$key]);
                    }
                }
            }
        }

        return $rates;
    }
}
