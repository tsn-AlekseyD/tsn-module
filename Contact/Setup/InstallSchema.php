<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;

use TSN\Contact\Api\Model\Schema\ContactInterface;

class InstallSchema implements InstallSchemaInterface
{
    /** {@inheritdoc} */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /** create table `tsn contacts` */
        $table = $installer->getConnection()->newTable(
            $installer->getTable(ContactInterface::TABLE_NAME)
        )->addColumn(
            ContactInterface::ID_FIELD,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned'=> true],
            'Contact ID'
        )->addColumn(
            ContactInterface::NAME_FIELD,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Name'
        )->addColumn(
            ContactInterface::EMAIL_FIELD,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Email'
        )->addColumn(
             ContactInterface::PHONE_FIELD,
             Table::TYPE_TEXT,
             255,
              ['nullable' => false],
              'Phone'
        )->addColumn(
            ContactInterface::COMMENTS_FIELD,
            Table::TYPE_TEXT,
            '2M',
            [],
            'Comments'
        )->addColumn(
                ContactInterface::STATUS_FIELD,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned'=> true],
                'Status'
            )
            ->addIndex(
            $setup->getIdxName(
                $installer->getTable(ContactInterface::TABLE_NAME),
                [ContactInterface::NAME_FIELD, ContactInterface::COMMENTS_FIELD],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            [ContactInterface::NAME_FIELD, ContactInterface::COMMENTS_FIELD],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )->setComment(
            'TSN-Media Contact Table'
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
