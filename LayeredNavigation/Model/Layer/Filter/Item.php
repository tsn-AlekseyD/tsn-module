<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_LayeredNavigation
 */

namespace TSN\LayeredNavigation\Model\Layer\Filter;
use Magento\Framework\App\ObjectManager;

class Item extends \Magento\Catalog\Model\Layer\Filter\Item {
	public function getRemoveUrl(){
		return $this->_url->getRemoveFilterUrl(
			$this->getFilter()->getRequestVar(),
			$this->getValue(),
			[$this->_htmlPagerBlock->getPageVarName() => null]
		);
	}
	public function getUrl(){
		return $this->_url->getFilterUrl(
			$this->getFilter()->getRequestVar(),
			$this->getValue(),
			[$this->_htmlPagerBlock->getPageVarName() => null],
			false
		);
	}
	public function isActive(){
		$values = ObjectManager::getInstance()->create(
				\TSN\LayeredNavigation\Model\Url\Builder::class
			)
			->getValuesFromUrl($this->getFilter()->getRequestVar());
		if(!empty($values)){
			return in_array($this->getValue(), $values);
		}
	}
}