<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Controller\Adminhtml\Contact;

use TSN\Contact\Api\Model\ContactInterface;
use TSN\Contact\Controller\Adminhtml\Contact as BaseAction;

class Create extends BaseAction
{
    const ACL_RESOURCE      = 'TSN_Contact::create';
    const MENU_ITEM         = 'TSN_Contact::create';
    const PAGE_TITLE        = 'Add Contact';
    const BREADCRUMB_TITLE  = 'Add Contact';

    public function execute()
    {
        $model = $this->getModel();

        $data = $this->_session->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }
        $this->registry->register(ContactInterface::REGISTRY_KEY, $model);

        return parent::execute();
    }
}
