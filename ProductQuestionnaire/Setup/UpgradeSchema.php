<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;

use TSN\ProductQuestionnaire\Api\Model\Schema\GroupQuestionnaireInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            /** create table 'tsn_questionnaire_group' */
            $table = $installer->getConnection()->newTable(
                $installer->getTable(GroupQuestionnaireInterface::TABLE_NAME)
            )->addColumn(
                GroupQuestionnaireInterface::ID_FIELD,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Group of Product Questionnaire Record Id'
            )->addColumn(
                GroupQuestionnaireInterface::GROUP_CODE,
                Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'un'],
                'Code of Group'
            )->addColumn(
                GroupQuestionnaireInterface::GROUP_LABEL,
                Table::TYPE_TEXT,
                '255',
                ['nullable' => false],
                'Name of Questionnaire Group'
            )->addColumn(
                GroupQuestionnaireInterface::ACTIVE_FIELD,
                Table::TYPE_SMALLINT,
                null,
                [],
                'Active Status'
            )->addIndex(
                $installer->getIdxName(
                    $installer->getTable(GroupQuestionnaireInterface::TABLE_NAME),
                    [GroupQuestionnaireInterface::GROUP_CODE],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                [GroupQuestionnaireInterface::GROUP_CODE],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )->addIndex(
                $setup->getIdxName(
                    $installer->getTable(GroupQuestionnaireInterface::TABLE_NAME),
                    [
                        GroupQuestionnaireInterface::GROUP_CODE,
                        GroupQuestionnaireInterface::GROUP_LABEL
                    ],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                [
                    GroupQuestionnaireInterface::GROUP_CODE,
                    GroupQuestionnaireInterface::GROUP_LABEL
                ],
                ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
            )->setComment(
                'Row Data Table'
            );

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}