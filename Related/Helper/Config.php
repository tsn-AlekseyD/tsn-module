<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Related\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * @package TSN\Related\Helper
 */
class Config extends AbstractHelper
{
    const RELATED_PRODUCT_QTY = 'related/customer_product_related/related_qty_product';

    /**
     * @return mixed
     */
     public function getRelatedProductQty()
     {
         return $this->scopeConfig->getValue(
             self::RELATED_PRODUCT_QTY,
             ScopeInterface::SCOPE_STORE
         );
     }
}
