<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_LayeredNavigation
 */

namespace TSN\LayeredNavigation\Plugin\Model\Adapter\Mysql\Filter;

use Magento\CatalogSearch\Model\Adapter\Mysql\Filter\Preprocessor as MagentoPreprocessor;
use Magento\Framework\Search\Request\FilterInterface;

use TSN\LayeredNavigation\Model\Url\Builder;

class Preprocessor {
	protected $urlBuilder;
	public function __construct(
		Builder $urlBuilder
	){
		$this->urlBuilder = $urlBuilder;
	}
	public function aroundProcess(
        MagentoPreprocessor $subject,
		\Closure $proceed,
        FilterInterface $filter,
		$isNegation,
		$query
	){
		if($filter->getField() === 'price'){
			$values = $this->urlBuilder->getValuesFromUrl('price');
			if(!empty($values)){
				$statements = [];
				foreach ($values as $value) {
					list($from, $to) = explode("-", $value);
					$statement = [
						$this->getSqlStringByArray(
							[floatval($from)],
							'final_price',
							'>='
						),
						$this->getSqlStringByArray(
							[floatval($to)],
							'final_price',
							'<='
						)
					];
					$statements[] = '('.implode(" AND ", $statement).')';
				}
				return implode(" OR ", $statements);
			}
		}
		if($filter->getField() === 'category_ids'){
			if(is_array($filter->getValue())){
				if(isset($filter->getValue()['in'])){
					return $this->getSqlStringByArray($filter->getValue()['in']);
				}
				return $this->getSqlStringByArray($filter->getValue());
			}
			elseif(is_string($filter->getValue())){
				return $this->getSqlStringByArray([$filter->getValue()]);
			}
		}
		return $proceed($filter, $isNegation, $query);
	}
	private function getSqlStringByArray(
		$array = [],
		$field = 'category_ids_index.category_id',
		$operator = '=',
		$rule = 'OR'
	){
		$statements = [];
		if(!empty($array)){
			foreach ($array as $value) {
				$statements[] = $field.' '.$operator.' '.$value;
			}
		}
		return implode(' '.$rule.' ', $statements);
	}
}