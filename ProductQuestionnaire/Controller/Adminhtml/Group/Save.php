<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Controller\Adminhtml\Group;

use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Message\Manager;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\LocalizedException;

use TSN\ProductQuestionnaire\Api\Model\GroupQuestionnaireRepositoryInterface;
use TSN\ProductQuestionnaire\Api\Model\GroupQuestionnaireInterface;
use TSN\ProductQuestionnaire\Api\Model\GroupQuestionnaireInterfaceFactory;
use TSN\ProductQuestionnaire\Controller\Adminhtml\Group;
use TSN\ProductQuestionnaire\Api\Model\Schema\GroupQuestionnaireInterface as SchemaInterface;

class Save extends Group
{
    /**
     * @var Manager
     */
    protected $messageManager;

    /**
     * @var GroupQuestionnaireRepositoryInterface
     */
    protected $groupQuestionRepository;

    /**
     * @var GroupQuestionnaireInterfaceFactory
     */
    protected $groupQuestionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    public function __construct(
        Registry $registry,
        GroupQuestionnaireRepositoryInterface $groupQuestionRepository,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        Manager $messageManager,
        GroupQuestionnaireInterfaceFactory $groupQuestionFactory,
        DataObjectHelper $dataObjectHelper,
        Context $context
    ) {
        $this->messageManager   = $messageManager;
        $this->groupQuestionFactory      = $groupQuestionFactory;
        $this->groupQuestionRepository   = $groupQuestionRepository;
        $this->dataObjectHelper  = $dataObjectHelper;
        parent::__construct($registry, $groupQuestionRepository, $resultPageFactory, $resultForwardFactory, $context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $id = $this->getRequest()->getParam(SchemaInterface::ID_FIELD);
            if ($id) {
                $model = $this->groupQuestionRepository->getById($id);
            } else {
                unset($data[SchemaInterface::ID_FIELD]);
                $model = $this->groupQuestionFactory->create();
            }

            try {
                $this->dataObjectHelper->populateWithArray($model, $data, GroupQuestionnaireInterface::class);
                $this->groupQuestionRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved this group.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', [SchemaInterface::ID_FIELD => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', [SchemaInterface::ID_FIELD => $this->getRequest()->getParam(SchemaInterface::ID_FIELD)]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
