<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Controller\Adminhtml\Contact;

use Magento\Framework\Exception\NoSuchEntityException;

use TSN\Contact\Api\Model\ContactInterface;
use TSN\Contact\Controller\Adminhtml\Contact as BaseAction;

class Edit extends BaseAction
{
    const ACL_RESOURCE      = 'TSN_Contact::edit';
    const MENU_ITEM         = 'TSN_Contact::edit';
    const PAGE_TITLE        = 'Edit Contact';
    const BREADCRUMB_TITLE  = 'Edit Contact';

    /** {@inheritdoc} */
    public function execute()
    {
        $id = $this->getRequest()->getParam(static::QUERY_PARAM_ID);

        if (!empty($id)) {
            try {
                $model = $this->repository->getById($id);
            } catch (NoSuchEntityException $exception) {
                $this->logger->error($exception->getMessage());
                $this->messageManager->addErrorMessage(__('Entity with id %1 not found', $id));
                return $this->redirectToGrid();
            }

        } else {
            $this->logger->error(
                sprintf("Require parameter `%s` is missing", static::QUERY_PARAM_ID)
            );
            $this->messageManager->addErrorMessage("Contact not found");
            return $this->redirectToGrid();
        }

        $data = $this->_session->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        $this->registry->register(ContactInterface::REGISTRY_KEY, $model);

        return parent::execute();
    }
}
