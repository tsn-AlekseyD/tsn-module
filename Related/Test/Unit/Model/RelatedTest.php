<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Related\Test\Unit\Model;

use PHPUnit\Framework\TestCase;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;

use TSN\Related\Model\Related;
use TSN\Related\Model\ResourceModel\Related as ResourceModelRelated;
use TSN\Related\Model\ResourceModel\Related\Collection as RelatedCollection;
use TSN\Related\Model\ResourceModel\RelatedFactory as ResourceRelatedFactory;

/**
 * Class RelatedTest
 * @package TSN\Related\Test\Unit\Model
 */
class RelatedTest extends TestCase
{
    /**
     * @var Related
     */
    protected $_related;

    /** {@inheritdoc} */
    protected function setUp()
    {
        $objectManager = new ObjectManager($this);

        $productCollection = $this->createMock(ProductCollection::class);
        $relatedCollection = $this->createMock(RelatedCollection::class);
        $resourceRelated = $this->createMock(ResourceModelRelated::class);

        $resourceRelated->expects($this->any())
            ->method('getRelatedProductCollection')
            ->willReturn($productCollection);

        $resourceRelatedFactory = $this->createMock(ResourceRelatedFactory::class,
            ['relatedCollection' => $relatedCollection]
        );

        $resourceRelatedFactory->expects($this->any())
            ->method('create')
            ->willReturn($resourceRelated);

        $this->_related = $objectManager->getObject(Related::class,
            ['resourceRelatedFactory' => $resourceRelatedFactory]
        );
    }

    /** {@inheritdoc} */
    protected function tearDown()
    {
        unset($this->_related);
    }

    /**
     * @assert ProductCollection
     */
    public function testGetRelatedProductCollection()
    {
        $result = $this->_related->getRelatedProductCollection();

        $this->assertInstanceOf(
            ProductCollection::class,
            $result
        );
    }
}
