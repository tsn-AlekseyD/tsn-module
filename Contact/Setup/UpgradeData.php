<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use TSN\Contact\Api\Model\ContactStatusRepositoryInterface;

class UpgradeData implements UpgradeDataInterface
{
    /** @var ContactStatusRepositoryInterface */
    protected $repository;

    public function __construct(
        ContactStatusRepositoryInterface $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $version = $context->getVersion();

        if(version_compare($version, "1.0.2", "<=")){
            $this->createStatuses();
        }
    }

    protected function createStatuses(){
        $statuses = [
            [
                'status_label'  => 'New',
                'is_default'    => '1'
            ],
            [
                'status_label'  => 'Pending',
                'is_default'    => '0'
            ],
            [
                'status_label'  => 'Canceled',
                'is_default'    => '0'
            ]
        ];

        foreach ($statuses as $statusData){
            $status = $this->repository->getContactStatusObject();
            $status->setLabel($statusData['status_label']);
            $status->setIsDefault($statusData['is_default']);

            $this->repository->save($status);
        }
    }
}
