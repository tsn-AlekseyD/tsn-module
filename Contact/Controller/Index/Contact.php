<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Controller\Index;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Translate\Inline\StateInterface;


use TSN\Contact\Api\Model\ContactRepositoryInterface;
use TSN\Contact\Api\Model\Schema\ContactInterface as ContactSchemaInterface;

class Contact extends \Magento\Framework\App\Action\Action
{
    /** @var DataPersistorInterface */
    protected $dataPersistor;

    /** @var StateInterface */
    protected $inlineTranslation;

    /** @var ContactRepositoryInterface */
    protected $contactRepository;

    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        ContactRepositoryInterface $contactRepository
    ) {
        parent::__construct($context);
        $this->inlineTranslation               = $inlineTranslation;
        $this->contactRepository               = $contactRepository;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        if (!$data['tsn']) {
            $this->_redirect('*/*/index');

            return;
        }
        $data = $data['tsn'];

        $this->inlineTranslation->suspend();
        try {

            $error = false;

            if (!\Zend_Validate::is(trim($data[ContactSchemaInterface::NAME_FIELD]), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data[ContactSchemaInterface::EMAIL_FIELD]), 'EmailAddress')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data[ContactSchemaInterface::PHONE_FIELD]), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data[ContactSchemaInterface::COMMENTS_FIELD]), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data[ContactSchemaInterface::STATUS_FIELD]), 'NotEmpty')) {
                $error = true;
            }
            if ($error) {
                throw new \Exception();
            }


            $contactObject = $this->contactRepository->getContactObject();
            $contactObject->setData(
                ContactSchemaInterface::NAME_FIELD,
                $data[ContactSchemaInterface::NAME_FIELD]);
            $contactObject->setData(
                ContactSchemaInterface::EMAIL_FIELD,
                $data[ContactSchemaInterface::EMAIL_FIELD]);
            $contactObject->setData(
                ContactSchemaInterface::PHONE_FIELD,
                $data[ContactSchemaInterface::PHONE_FIELD]);
            $contactObject->setData(
                ContactSchemaInterface::COMMENTS_FIELD,
                $data[ContactSchemaInterface::COMMENTS_FIELD]);
            $contactObject->setData(
                ContactSchemaInterface::STATUS_FIELD,
                $data[ContactSchemaInterface::STATUS_FIELD]);

            $this->contactRepository->save($contactObject);

            $this->messageManager->addSuccessMessage( __('Thanks for your valuable feedback.') );



            $this->_redirect('*/*/index');
            return;
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();
            $this->messageManager->addErrorMessage(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            $this->getDataPersistor()->set('tsn_contact', $data);
            $this->_redirect('*/*/index');
            return;
        }
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }
}