<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_LayeredNavigation
 */

namespace TSN\LayeredNavigation\Model\ResourceModel\Fulltext;

use Magento\Framework\App\ObjectManager;
use Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection as MagentoCollection;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;

class Collection extends MagentoCollection {
	protected $_addedFilters = [];
	public function addFieldToFilter($field, $condition = null){
		if (is_string($field)) {
			$this->_addedFilters[$field] = $condition;
		}
		return parent::addFieldToFilter($field, $condition);
	}
	public function addCategoriesFilter(array $categoriesFilter){
		$this->addFieldToFilter('category_ids', $categoriesFilter);
		return $this;
	}
	public function getAddedFilters(){
		return $this->_addedFilters;
	}
	public function updateSearchCriteriaBuilder(){
		$searchCriteriaBuilder = ObjectManager::getInstance()
			->create(SearchCriteriaBuilder::class);
		$this->setSearchCriteriaBuilder($searchCriteriaBuilder);
		return $this;
	}
	protected function _prepareStatisticsData(){
		$this->_renderFilters();
		return parent::_prepareStatisticsData();
	}
}