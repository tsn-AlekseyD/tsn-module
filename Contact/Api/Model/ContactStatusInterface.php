<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Api\Model;

interface ContactStatusInterface
{
    const CACHE_TAG = 'tsn_contact_status';
    /**
     * @return int|null
     */
    public function getId();

    /**
     * @param int $contactId
     * @return ContactStatusInterface
     */
    public function setId($contactId);

    /**
     * @return string
     */
    public function getLabel();

    /**
     * @param string $label
     * @return ContactStatusInterface
     */
    public function setLabel($label);

    /**
     * @return int
     */
    public function getIsDefault();

    /**
     * @param int $isDefault
     * @return ContactStatusInterface
     */
    public function setIsDefault($isDefault);

    /**
     * @return bool
     */
    public function isDefault();
}
