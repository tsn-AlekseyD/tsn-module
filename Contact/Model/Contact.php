<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

use TSN\Contact\Api\Model\ContactInterface;
use TSN\Contact\Api\Model\Schema\ContactInterface as SchemaInterface;
use TSN\Contact\Model\ResourceModel\Contact as ResourceModel;

class Contact extends AbstractModel implements ContactInterface, IdentityInterface
{
    /** {@inheritdoc} */
    public function getName()
    {
        return $this->getData(SchemaInterface::NAME_FIELD);
    }

    /** {@inheritdoc} */
    public function setName($name)
    {
        $this->setData(SchemaInterface::NAME_FIELD, $name);

        return $this;
    }

    /** {@inheritdoc} */
    public function getEmail()
    {
        return $this->getData(SchemaInterface::EMAIL_FIELD);
    }

    /** {@inheritdoc} */
    public function setEmail($email)
    {
        $this->setData(SchemaInterface::EMAIL_FIELD, $email);

        return $this;
    }

    /** {@inheritdoc} */
    public function getPhone()
    {
        return $this->getData(SchemaInterface::PHONE_FIELD);
    }

    /** {@inheritdoc} */
    public function setPhone($phone)
    {
        $this->setData(SchemaInterface::PHONE_FIELD, $phone);

        return $this;
    }

    /** {@inheritdoc} */
    public function getComment()
    {
        return $this->getData(SchemaInterface::COMMENTS_FIELD);
    }

    /** {@inheritdoc} */
    public function setComment($comment)
    {
        $this->setData(SchemaInterface::COMMENTS_FIELD, $comment);

        return $this;
    }

    /** {@inheritdoc} */
    public function getIdentities()
    {
        return [sprintf('%s_%d', ContactInterface::CACHE_TAG, (int) $this->getId())];
    }

    /** {@inheritdoc} */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
