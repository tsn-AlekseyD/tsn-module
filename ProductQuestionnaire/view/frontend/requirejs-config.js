var config = {
    map: {
        '*': {
            'tsn/swatchRenderer':    	'TSN_ProductQuestionnaire/js/swatch-renderer',
            'sidebar':                  'TSN_ProductQuestionnaire/js/sidebar',
            'tsn/ajaxToCart':       	'TSN_ProductQuestionnaire/js/questionnaire-popup',
            'tsn/ajaxsuite':            'TSN_ProductQuestionnaire/js/popup-style'
        }
    }
};

