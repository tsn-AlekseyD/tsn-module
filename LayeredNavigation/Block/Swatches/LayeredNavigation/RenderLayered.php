<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_LayeredNavigation
 */

namespace TSN\LayeredNavigation\Block\Swatches\LayeredNavigation;

use Magento\Swatches\Block\LayeredNavigation\RenderLayered as MagentoRenderLayered;
use Magento\Framework\View\Element\Template\Context;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Catalog\Model\ResourceModel\Layer\Filter\AttributeFactory;
use Magento\Swatches\Helper\Data;
use Magento\Swatches\Helper\Media;

use TSN\LayeredNavigation\Model\Url\Builder;

class RenderLayered extends MagentoRenderLayered {
	protected $urlBuilder;
	public function __construct(
		Context $context,
		Attribute $eavAttribute,
		AttributeFactory $layerAttribute,
		Data $swatchHelper,
		Media $mediaHelper,
		Builder $urlBuilder,
		array $data = []
	) {
		$this->urlBuilder = $urlBuilder;
		parent::__construct(
			$context,
			$eavAttribute,
			$layerAttribute,
			$swatchHelper,
			$mediaHelper,
			$data
		);
	}
	public function buildUrl($attributeCode, $optionId){
		if(in_array($optionId, $this->urlBuilder->getValuesFromUrl($attributeCode))){
			return $this->urlBuilder->getRemoveFilterUrl($attributeCode, $optionId);
		}
		else{
			return $this->urlBuilder->getFilterUrl($attributeCode, $optionId);
		}
	}
}