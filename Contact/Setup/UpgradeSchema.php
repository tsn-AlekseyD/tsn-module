<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;

use TSN\Contact\Api\Model\Schema\ContactStatusInterface;
/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /** create table `tsn status` */
        $table = $installer->getConnection()->newTable(
            $installer->getTable(ContactStatusInterface::TABLE_NAME)
        )->addColumn(
            ContactStatusInterface::ID_FIELD,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned'=> true],
            'Status Id'
        )->addColumn(
            ContactStatusInterface::LABEL_FIELD,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Status label'
        )->addColumn(
            ContactStatusInterface::IS_DEFAULT_FIELD,
            Table::TYPE_TEXT,
            255,
            [],
            'Is default'
        )
            ->addIndex(
                $setup->getIdxName(
                    $installer->getTable(ContactStatusInterface::TABLE_NAME),
                    [ContactStatusInterface::LABEL_FIELD],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                [ContactStatusInterface::LABEL_FIELD],
                ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
            )->setComment(
                'TSN Contact Status Table'
            );
        $installer->getConnection()->createTable($table);

        $setup->getConnection()->addForeignKey(
            $setup->getFkName(

                'tsn_contact',
                'status',
                'tsn_contact_status',
                'status_id'
            ),
            $setup->getTable('tsn_contact'),
            'status',
            $setup->getTable('tsn_contact_status'),
            'status_id',

            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->endSetup();
    }
}