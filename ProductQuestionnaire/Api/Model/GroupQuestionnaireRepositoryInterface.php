<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Api\Model;

use Magento\Framework\Api\SearchCriteriaInterface;

interface GroupQuestionnaireRepositoryInterface
{

    /**
     * @param int $groupId
     * @return GroupQuestionnaireInterface
     */
    public function getById($groupId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \TSN\ProductQuestionnaire\Api\Model\QuestionnaireSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param GroupQuestionnaireInterface $group
     * @return GroupQuestionnaireInterface
     */
    public function save(GroupQuestionnaireInterface $group);

    /**
     * @param GroupQuestionnaireInterface $group
     * @return GroupQuestionnaireRepositoryInterface
     */
    public function delete(GroupQuestionnaireInterface $group);

    /**
     * @param int $groupId
     * @return GroupQuestionnaireRepositoryInterface
     */
    public function deleteById($groupId);

    /**
     * @return GroupQuestionnaireInterface
     */
    public function getGroupQuestionnaireObject();
}