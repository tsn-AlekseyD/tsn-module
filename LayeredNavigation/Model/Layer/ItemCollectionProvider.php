<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_LayeredNavigation
 */

namespace TSN\LayeredNavigation\Model\Layer;

use Magento\Catalog\Model\Category;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Layer\ItemCollectionProviderInterface;

use TSN\LayeredNavigation\Model\ResourceModel\Fulltext\CollectionFactory;

class ItemCollectionProvider implements ItemCollectionProviderInterface {
	private $storeManager;
	private $collectionFactory;
	public function __construct(
		StoreManagerInterface $storeManager,
		CollectionFactory $collectionFactory
	){
		$this->storeManager = $storeManager;
		$this->collectionFactory = $collectionFactory;
	}
	public function getCollection(Category $category){
		if ($category->getId() == $this->storeManager->getStore()->getRootCategoryId()) {
			$collection = $this->collectionFactory->create(['searchRequestName' => 'quick_search_container']);
		} else {
			$collection = $this->collectionFactory->create();
			$collection->addCategoryFilter($category);
		}
		return $collection;
	}
}