<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Api\Model\Schema;


interface ContactStatusInterface
{
    const TABLE_NAME        = 'tsn_contact_status';

    const ID_FIELD          = 'status_id';
    const LABEL_FIELD       = 'status_label';
    const IS_DEFAULT_FIELD  = 'is_default';
}

