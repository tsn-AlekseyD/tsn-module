<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Model\ResourceModel\GroupQuestionnaire;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

use TSN\ProductQuestionnaire\Model\GroupQuestionnaire as Model;
use TSN\ProductQuestionnaire\Model\ResourceModel\GroupQuestionnaire as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'questionnaire_group_id';

    /**
     * Collection initialisation
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(Model::class, ResourceModel::class);
    }

    public function toOptionArray()
    {
        $groupList = parent::_toOptionArray('code_group', 'label_group');
        array_unshift($groupList, ['value'=>'default', 'label' =>'Default' ]);
        return $groupList;
    }
}