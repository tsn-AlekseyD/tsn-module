<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Block\Adminhtml\Contact\Edit\Tab;

use TSN\Contact\Api\Model\Schema\ContactInterface as SchemaInterface;

class General extends AbstractTab
{
    const TAB_LABEL     = 'General';
    const TAB_TITLE     = 'General';

    protected $selectStatusHelper;

    /** {@inheritdoc} */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('contact_');
        $form->setFieldNameSuffix('contact');

        $fieldSet = $form->addFieldset(
            'general_fieldset',
            ['legend' => __('General')]
        );

        if ($this->model->getData(SchemaInterface::ID_FIELD)) {
            $fieldSet->addField(
                SchemaInterface::ID_FIELD,
                'hidden',
                [
                    'name' => SchemaInterface::ID_FIELD
                ]
            );
        }

        $fieldSet->addField(
            SchemaInterface::NAME_FIELD,
            'text',
            [
                'name'      => SchemaInterface::NAME_FIELD,
                'label'     => __('Name'),
                'required'  => true
            ]
        );

        $fieldSet->addField(
            SchemaInterface::EMAIL_FIELD,
            'text',
            [
                'name'      => SchemaInterface::EMAIL_FIELD,
                'label'     => __('Email'),
                'required'  => true
            ]
        );

        $fieldSet->addField(
            SchemaInterface::PHONE_FIELD,
            'text',
            [
                'name'      => SchemaInterface::PHONE_FIELD,
                'label'     => __('Phone'),
                'required'  => true
            ]
        );

        $fieldSet->addField(
            SchemaInterface::COMMENTS_FIELD,
            'editor',
            [
                'name'      => SchemaInterface::COMMENTS_FIELD,
                'label'     => __('Comments'),
                'required'  => true,
                'config'    => $this->wysiwygConfig->getConfig()
            ]
        );

        $fieldSet->addField(
            SchemaInterface::STATUS_FIELD,
            'text',
            [
                'name'      => SchemaInterface::STATUS_FIELD,
                'label'     => __('Status'),
                'required'  => true
            ]
        );

//        $fieldSet->addField(
//            SchemaInterface::STATUS_FIELD,
//            'select',
//            [
//                'name'      => SchemaInterface::STATUS_FIELD,
//                'label'     => __('Status'),
//                'values' => $this->helperSelectForm->getReviewStatuses(),
//                'required' => true
//            ]
//        );

        $data = $this->model->getData();

        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
