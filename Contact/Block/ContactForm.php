<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

use TSN\Contact\Api\Model\ContactStatusInterface;


class ContactForm extends Template
{
    /** @var ContactStatusInterface */
    protected $_contactStatus;

    public function __construct(
        Context $context,
        ContactStatusInterface $contactStatus,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_isScopePrivate  = true;
        $this->_contactStatus   = $contactStatus;
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('tsn_contact/index/contact', ['_secure' => true]);
    }

    /**
     * @return int
     */
    public function isDefault()
    {
        if (!empty($this->_contactStatus)) {
            $defaultContactStatus = $this->_contactStatus
                                         ->getCollection()
                                         ->addFieldToFilter('is_default', 1);

            foreach ($defaultContactStatus as $item)
            {
                 return $item->getId();
            }
        }
    }

    public function toHtml()
    {
        if(!$this->_scopeConfig->getValue('tsn_contact/general/is_enabled', ScopeConfigInterface::SCOPE_TYPE_DEFAULT )) {
            return '';
        }

        return parent::toHtml();
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}