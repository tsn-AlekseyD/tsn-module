<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

use TSN\Contact\Api\Model\ContactStatusInterface;
use TSN\Contact\Api\Model\Schema\ContactStatusInterface as SchemaInterface;
use TSN\Contact\Model\ResourceModel\ContactStatus as ResourceModel;

class ContactStatus extends AbstractModel implements ContactStatusInterface, IdentityInterface
{
    /** {@inheritdoc} */
    public function getLabel()
    {
        return $this->getData(SchemaInterface::LABEL_FIELD);
    }

    /** {@inheritdoc} */
    public function setLabel($label)
    {
        $this->setData(SchemaInterface::LABEL_FIELD, $label);

        return $this;
    }

    /** {@inheritdoc} */
    public function getIsDefault()
    {
        return $this->getData(SchemaInterface::IS_DEFAULT_FIELD);
    }

    /** {@inheritdoc} */
    public function setIsDefault($isDefault)
    {
        $this->setData(SchemaInterface::IS_DEFAULT_FIELD, $isDefault);

        return $this;
    }

    /** {@inheritdoc} */
    public function isDefault()
    {
        return (bool) $this->getIsDefault();
    }

    /** {@inheritdoc} */
    public function getIdentities()
    {
        return [sprintf('%s_%d', ContactStatusInterface::CACHE_TAG, (int) $this->getId())];
    }

    /** {@inheritdoc} */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}