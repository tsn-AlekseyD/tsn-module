<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class ContactGrid extends Container
{
    /** {@inheritdoc} */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_contact';
        $this->_blockGroup = 'TSN_Contact';
        $this->_headerText = __('Contact');
        $this->_addButtonLabel = __('Create new');
        parent::_construct();
    }

    /** {@inheritdoc} */
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/create');
    }
}
