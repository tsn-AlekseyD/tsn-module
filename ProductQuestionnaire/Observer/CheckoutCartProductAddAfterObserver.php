<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class CheckoutCartProductAddAfterObserver implements ObserverInterface
{

    public function execute(Observer $observer) {
        if(isset($_POST['tsn']) || !empty($_POST['tsn'])) {
            $dataPostArray = $_POST['tsn'];

            $html = "";
            if (is_array($dataPostArray) || sizeof($dataPostArray)) {

                foreach ($dataPostArray as $question) {
                    $html .= '<div><h4>' . $question['question'] . '</h4><p>' . $question['answer'] . '</p></div>';
                }
            } else {
                $html .= '<div><h4>No date</h4></div>';
            }

            $item = $observer->getEvent()->getData('quote_item');

            $item = ($item->getParentItem() ? $item->getParentItem() : $item);

            $item->setQuoteQuestion($html);

            $item->getProduct()->setIsSuperMode(true);
        }
    }
}
