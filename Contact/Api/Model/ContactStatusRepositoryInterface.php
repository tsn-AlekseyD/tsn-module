<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */


namespace TSN\Contact\Api\Model;



use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ContactStatusRepositoryInterface
{
    /**
     * @param int $contactStatusId
     * @return ContactStatusInterface
     */
    public function get($contactStatusId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param ContactStatusInterface $contact
     * @return ContactStatusInterface
     */
    public function save(ContactStatusInterface $contact);

    /**
     * @param ContactStatusInterface $contact
     * @return ContactStatusRepositoryInterface
     */
    public function delete(ContactStatusInterface $contact);

    /**
     * @param int $contactStatusId
     * @return ContactStatusRepositoryInterface
     */
    public function deleteById($contactStatusId);

    /**
     * @return ContactStatusInterface
     */
    public function getContactStatusObject();

}