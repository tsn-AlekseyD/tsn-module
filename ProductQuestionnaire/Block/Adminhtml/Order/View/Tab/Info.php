<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Block\Adminhtml\Order\View\Tab;

/**
 * Order information tab
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Info extends \Magento\Sales\Block\Adminhtml\Order\View\Tab\Info
{
    public function getQuestionHtml()
    {
        return $this->getChildHtml('order_product_question');
    }

}
