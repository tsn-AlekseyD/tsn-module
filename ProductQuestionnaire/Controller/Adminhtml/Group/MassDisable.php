<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_ProductQuestionnaire
 */

namespace TSN\ProductQuestionnaire\Controller\Adminhtml\Group;

use TSN\ProductQuestionnaire\Model\GroupQuestionnaire;

class MassDisable extends MassAction
{
    /**
     * @param GroupQuestionnaire $data
     * @return $this
     */
    protected function massAction(GroupQuestionnaire $data)
    {
        $data->setIsActive(false);
        $this->groupQuestionRepository->save($data);
        return $this;
    }
}
