<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_LayeredNavigation
 */
namespace TSN\LayeredNavigation\Block;

use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\LayeredNavigation\Block\Navigation\FilterRenderer as MagentoFilterRenderer;

class FilterRenderer extends MagentoFilterRenderer {

    /**
     * @param FilterInterface $filter
     * @return string
     */
    public function render(FilterInterface $filter) {
        $this->assign('filterItems', $filter->getItems());
        $this->assign('filter', $filter);
        $html = $this->_toHtml();
        $this->assign('filterItems', []);
        return $html;
    }

    public function getPriceRange($filter) {
        $filterprice = array('min' => 0, 'max' => 0);
        $priceArr = $filter->getResource()->loadPrices(10000000000);
        $filterprice['min'] = reset($priceArr);
        $filterprice['max'] = end($priceArr);
        return $filterprice;
    }

    public function getFilterUrl($filter) {
        $query = ['price' => ''];
        return $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true, '_query' => $query]);
    }

}
