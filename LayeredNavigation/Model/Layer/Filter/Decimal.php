<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_LayeredNavigation
 */

namespace TSN\LayeredNavigation\Model\Layer\Filter;

class Decimal extends \Magento\CatalogSearch\Model\Layer\Filter\Decimal {}