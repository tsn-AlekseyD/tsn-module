<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Api\Model;


use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ContactRepositoryInterface
{
//    /**
//     * @param int $contactId
//     * @return ContactInterface
//     */
//    public function get($contactId);

    /**
     * @param int $contactId
     * @return ContactInterface
     */
    public function getById($contactId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param ContactInterface $contact
     * @return ContactInterface
     */
    public function save(ContactInterface $contact);

    /**
     * @param ContactInterface $contact
     * @return ContactRepositoryInterface
     */
    public function delete(ContactInterface $contact);

    /**
     * @param int $contactId
     * @return ContactRepositoryInterface
     */
    public function deleteById($contactId);

    /**
     * @return ContactInterface
     */
    public function getContactObject();

}