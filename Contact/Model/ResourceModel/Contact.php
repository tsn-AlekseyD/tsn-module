<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

use TSN\Contact\Api\Model\Schema\ContactInterface;

class Contact extends AbstractDb
{
    /** {@inheritdoc} */
    protected function _construct()
    {
        $this->_init(ContactInterface::TABLE_NAME, ContactInterface::ID_FIELD);
    }
}