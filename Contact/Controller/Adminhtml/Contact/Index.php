<?php
/**
 * @author TSN-Media Team
 * @copyright Copyright (c) 2018 TSN-Media (https://tsn-media.com)
 * @package TSN_Base
 */

namespace TSN\Contact\Controller\Adminhtml\Contact;

use TSN\Contact\Controller\Adminhtml\Contact as BaseAction;

class Index extends BaseAction
{
    const ACL_RESOURCE      = 'TSN_Contact::grid';
    const MENU_ITEM         = 'TSN_Contact::grid';
    const PAGE_TITLE        = 'Contact Grid';
    const BREADCRUMB_TITLE  = 'Contact Grid';
}
